#!/bin/bash

sudo rmmod openxtfb

if [ "$1" != "unload" ]; then
	sudo insmod openxtfb.ko
fi
