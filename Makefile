ifneq ($(KERNELRELEASE),)
# kbuild part of makefile
include Kbuild

else
# normal makefile
KERNEL_VERSION ?= `uname -r`
KERNEL_SRC ?= /lib/modules/$(KERNEL_VERSION)/build
INSTALL_HDR_PATH ?= /usr

IVC_INCLUDE_DIR ?= /usr/src/ivc/include
PDH_INCLUDE_DIR ?= /usr/src/pv_display_helper/include

default:
	$(MAKE) -C $(KERNEL_SRC) M=$$PWD

clean:
	$(MAKE) -C $(KERNEL_SRC) M=$$PWD clean

modules_install:
	$(MAKE) -C $(KERNEL_SRC) M=$$PWD modules_install

headers_install:
	install -d $(INSTALL_HDR_PATH)/include/linux
	install -m 0644 openxtfb.h $(INSTALL_HDR_PATH)/include/linux/openxtfb.h

endif
